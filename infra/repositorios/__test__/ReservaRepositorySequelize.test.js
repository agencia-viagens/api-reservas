const ReservaRepositorySequelize = require('../ReservaRepositorySequelize')

describe('Quando o cliente criar uma reserva', () => {
    test('Então o sistema deve enviar uma notificação para o cliente', async () => {
        const reservaModelMock = {
            create: jest.fn(() => Promise.resolve({id: 123}))
        }
        const mensageriaMock = {
            publish: jest.fn(() => Promise.resolve())
        }
        const fileStorageMock = {
            saveFile: jest.fn(() => Promise.resolve())
        }

        const reservaRepository = ReservaRepositorySequelize(
            reservaModelMock, mensageriaMock, fileStorageMock)

        const novaReserva = await reservaRepository.inserir({})

        expect(reservaModelMock.create).toHaveBeenCalled()
        expect(mensageriaMock.publish).toHaveBeenCalled()
        expect(novaReserva.id).toBe(123)
    })
})