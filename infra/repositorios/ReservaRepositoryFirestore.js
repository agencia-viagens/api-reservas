module.exports = (firestore) => ({
  inserir: async (reserva) => {
    const res = await firestore
                  .collection('reservas')
                  .add(reserva)

    const novaReserva = await res.get()

    return {
      id: res.id,
      ...novaReserva.data()
    }
  },
  
  consultar: async () => {
    const query = await firestore
                    .collection('reservas')
                    .get()
    
    if(query.empty) {
      return []
    } else {
      return query.docs.map(doc => {
        const data = doc.data()

        return {
          ...data,
          id: doc.id
        }
      })
    }

  },
  remover: async (id) => {
    const reserva = await firestore
                          .collection('reservas')
                          .doc(id)
                          .get()

    if(reserva.exists) {
      await reserva._ref.delete()
      return reserva.data()
    } else {
      return null
    }
  },
  alterar: async (id, novaReserva) => {
    const reserva = await firestore
                            .collection('reservas')
                            .doc(id)
                            .get()
    if(reserva.exists) {
      reserva._ref.set({
        ...novaReserva
      })

      return {
        id,
        ...novaReserva
      }
    } else {
      return null
    }
  }
})