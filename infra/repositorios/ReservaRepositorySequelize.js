module.exports = (reservaModel, mensageria, fileStorage) => ({
    inserir: async (reserva) => {
        const novaReserva = await reservaModel.create(reserva)

        // Publicação de uma mensagem em fila
        await mensageria.publish(
            'notificacao_reserva', 
            novaReserva
        )

        await fileStorage.saveFile(reserva.file, '/reserva/', 'tarley.jpg')

        return novaReserva
    },
    consultar: async () => {
        return await reservaModel.findAll()
    },
    remover: async (id) => {
        const reserva  = await reservaModel.findByPk(id)
        await reserva.destroy()

        return reserva
    },
    alterar: async (id, reserva) => {
        const novaReserva = await reservaModel.findByPk(id)

        novaReserva.dataInicio = reserva.dataInicio
        novaReserva.dataFim = reserva.dataFim
        novaReserva.quantidadeQuartos = reserva.quantidadeQuartos
        novaReserva.quantidadeHospedes = reserva.quantidadeHospedes

        await novaReserva.save()

        return novaReserva
    }
})