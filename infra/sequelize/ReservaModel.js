const {DataTypes} = require('sequelize')

module.exports = (sequelize) => {
    const ReservaModel = sequelize.define('reserva', {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        dataInicio: {
            field: 'dta_inicio',
            type: DataTypes.DATE,
            allowNull: false,
            //defaultValue: DATETIME.NOW
        },
        dataFim: {
            field: 'dta_fim',
            type: DataTypes.DATE,
            allowNull: false
        },
        quantidadeQuartos: {
            field: 'qtd_quartos',
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        },
        quantidadeHospedes: {
            field: 'qtd_hospedes',
            type: DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 1
        }
    }, {
        freezeTableName: true,
    })

    return  ReservaModel
}