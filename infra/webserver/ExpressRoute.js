const express = require('express')

const app = express()

module.exports = (reservaController) => {
    app.get('/', reservaController.consultar)
    app.post('/', reservaController.inserir)
    app.delete('/:id', reservaController.excluir)
    app.put('/:id', reservaController.alterar)

    return app
}