const {PubSub} = require('@google-cloud/pubsub')

const pubSubClient = new PubSub({
    projectId: 'agencia-viagens-343722',
    credentials: {
        client_email: 'app-agencia@agencia-viagens-343722.iam.gserviceaccount.com',
        private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDKsc/zNUgKUVzM\nIDIAgKimsmvB30zOCOZFgvVyEtwG7/paCne/JzLsHq9NBmdYQPXl64I97Cb3bVSg\njQAEU/i+DVScqUtfvgS+6jUiQANmifLSHclDAva0yedfZfCNR8CVAkI1wJzKCntI\nELzE3TnO3nENTgJfNsUp2G9LfjZH9J/eplyDSj66NQsmv6FLS+KGw/Qrs/3fO+Jj\nci1r3iz5QhSg0i4cFxn+1g7Vz+VXnyIkBQYM/3BE9c62dioYfO/Di+lbZX9e6jWR\nFf9p5CrVB0Edk8NrIUCIXXUn7Xb12CT6ReK3UTGpM+PRbDp9eH4xF8xm7NseybBD\nChvj45GHAgMBAAECggEAB0pzZcF6NeTMpwsBbBiJ+HUH1ea8/IGVcX/5vtYEKWZj\nEES6yfAUWinrDvo2qBYDbPcWWDkxAvVhII/x0DYKXHZeShmiJxoj/DoFxJWeNFys\nlKI9pbYVlFXZh1+Yp3xNO7mOaCCQLd7ypZpJ2qBFKWEW7+4+Z5kGE9s/0EIEnHyV\nB6rlO9hd2+vQn/R0Z9EA87tsRhyQqgF5Ws60nw0f9AbfvYRqVLl4Btbdr51z5iI6\n3VLTB83uYo/XEHTIQ07/i9/ah9X3w7EtboM9FcoL0kHuxCKRs3Vb9ggbAFdic5DD\nMRVKaXuCBcLvZoM2Y24URwCh53ukBRvNdg+szBd5AQKBgQDtFume8cAW1fzy09CK\ntoRpp3SLXu3ofZ+X0Kj7ikYhZddtHjPo4NXC/57k6YIssIhN/iZW+hCb5ugpgL4f\nsfB/T+bAA0W+o0PM3FLeoJjidTVJgOd15pIxY8rR0n2atnjfcaui9EPul44jVjVb\nKQwS48qwiYL4583uJyR48nyhkwKBgQDa3JiWnDnA6PXdIJKGMJJkqms1qjibCeva\nnX6KvX6rTlTQvBXjg9Ho6xByMlVyws2+L/eM5/gA6UAlwcDwnfDoUh4YSr+mx2vK\nCvzyDN0QA/k9nRtzqy09ZiiI7ql5wVcCJcyMWP9FP3Gk0BIkHFYm/ELf6BsGCvwO\n8DCexOuYvQKBgEmRgoy/l0t+uy7eayef2xatz4wvuMMbuBnztXvYPErAv0gsxw7M\nNYh8qWF+09XqMWGCK4NxtRWrYP7/4/8Rx/65XHDYkxvErZU0CMLsMoSSuiZEyY3c\nLiEFRjo1hqQrWvZ63zeBdFLk1EZCyeuvoP97j7mX8fwqccwkeTzOgb3hAoGAF+Cl\nTRkyO9euqzdYhQdbkVv+YlZ14oRMlQdFvQE7hpj/K965TJ8fEZhDrRHMvL6AD9XP\n3jGYnxDTjMr8r8Ores0qt1/AVQPAwMQ+IHJC0/J7CLvROOdeAirZFUdkegIb6gIR\ntOYow3dOgLqqoAHvbEVtTlxWRmyAX2jZBRla0skCgYBcylQT7+yB82n52ptAqL68\nxA5xGEhO14fdFEy2WVvrE4U+rs+zrL6u6erzw8PGAzIAIWdj2W6p1GW+2PG++AuK\n3S60u9D4Z8R2B+RqxeqSXqI+W1R5yd87U18kct0TyGGEnPrfrZZL8fM3UsnOqhHC\nuaRONDzcPuxqwH5h8ib/Xg==\n-----END PRIVATE KEY-----\n'
    }
})

module.exports = () => ({
    publish: async (nomeTopico, data) => {
        console.log('Iniciando publish')
        const dataBuffer = Buffer.from(JSON.stringify(data))

        const messageId = await pubSubClient
                .topic(nomeTopico)
                .publishMessage({data: dataBuffer})

        console.log('Mensagem publicada com sucesso' 
            + messageId
        )
    }
})