const fs = require('fs');

module.exports = (cloudStorage) => ({
    saveFile: async (base64Text, dir, filename) => {
        const buffer = Buffer.from(base64Text, 'base64');
        const data = buffer.toString()

        fs.writeFileSync('/tmp/' + filename, data)

        await cloudStorage
                .bucket('ex_int_1')
                .upload('/tmp/'+ filename, {
                    destination: '/reservas/' + filename
                })
        
        fs.unlinkSync('/tmp/' + filename)
    }
})