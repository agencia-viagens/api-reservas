const awilix = require('awilix')

const reservaRepositoryFirestore = require('../repositorios/ReservaRepositoryFirestore')
const reservaRepositorySequelize = require('../repositorios/ReservaRepositorySequelize')

const container = awilix.createContainer({
    injectionMode: awilix.InjectionMode.CLASSIC
})

module.exports = () => {
    container.register({
        reservaRepository: awilix
                            //.asFunction(reservaRepositoryFirestore)
                            .asFunction(reservaRepositorySequelize)
                            .setLifetime(awilix.Lifetime.SINGLETON)
    })

    container.loadModules([
        'portas/**/*.js',
        'aplicacao/**/*.js',
        'infra/firestore/**/*.js',
        'infra/webserver/**/*.js',
        'infra/sequelize/**/*.js',
        'infra/pubsub/**/*.js',
        'infra/storage/**/*.js'
    ], {
        formatName: 'camelCase'
    })

    return container
}