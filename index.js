//const reservaController = require('./portas/http/ReservaController')
const Setup = require('./infra/config/Setup')

const container = Setup()
const app = container.resolve('expressRoute')

exports.reservas_v4 = app

/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
/*
exports.reservas_v4 = (req, res) => {
  const reservaController = container.resolve('reservaController')
  console.log("Requisição recebida " + req.method)

  switch(req.method) {
    case 'POST':
      reservaController.inserir(req, res)
      break
    case 'GET':
      reservaController.consultar(req, res)
      break
    default:
      res.status(404).send('Not Found!')
  }

  //let message = req.query.message || req.body.message || 'Hello World!';
  //res.status(200).send(message);
};
*/