let db = []

module.exports = (reservaRepository) => ({
    consultar: async (req, res) => {
        console.log('Consulta de reservas')

        const reservas = await reservaRepository.consultar()

        res.json({
            cod: 200, 
            msg: 'Consulta realizada com sucesso',
            data: reservas
         })
    },
    inserir: async (req, res) => {
        console.log('Registrando reserva')
        const reserva = req.body
        
        //db.push(reserva)
        const novaReserva = await reservaRepository.inserir(reserva)

        res.json({
            cod: 200, 
            msg: 'Reserva registrada com suceso',
            data: novaReserva
        })
    },
    excluir: async (req, res) => {
        const id = req.params.id
        console.log('ID=', id)

        const reserva =  await reservaRepository.remover(id)

        if(reserva) {
            res.json({
                cod: 200, 
                msg: 'Reserva excluida com sucesso',
                data: reserva
            })
        } else {
            res.status(404).json({
                cod: 404, 
                msg: 'Reserva não encontrada'
            })
        }
    },
    alterar: async (req, res) => {
        const id = req.params.id
        const novaReserva = req.body
        console.log('ID=', id)

        const reserva = await reservaRepository.alterar(id, novaReserva)

        if(reserva) {
            res.json({
                cod: 200, 
                msg: 'Reserva alterada com sucesso',
                data: reserva
            })
        } else {
            res.status(404).json({
                cod: 404, 
                msg: 'Reserva não encontrada'
            })
        }
    }
})